package chatClientPackage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Songyang WANG
 * @author Qiaodan SHEN
 * @version 1.0.0 28/03/2022 Cette classe est pour initialiser le client <br>
 *          En utilisant multi-thread il peut recevoir l'entrée sur le terminal
 *          et communiquer par channel en même temps il contient la fonction
 *          main() comme thread principal et MessageReceptor.start() comme
 *          thread secondaire
 */
public class ChatClient1 {

	public static class MessageReceptor extends Thread {
		private Socket client;
		private Boolean flag = true;

		public MessageReceptor(Socket client) {
			this.client = client;

		}

		/**
		 * Démarrer l'exécution du thread <br>
		 * chaque client utilise son propre socket pour communiquer avec le serveur <br>
		 * chaque fois qu'on reçoit un message depuis le socket, on l'affiche <br>
		 * chaque fois qu'on reçoit un message depuis le terminal, on l'envoie vers le serveur <br>
		 * Il termine si flag == false
		 */
		@Override
		public void run() {
			try {

				byte b[] = new byte[35];

				InputStream ins = client.getInputStream();
			
					try {
						InputStream in = client.getInputStream();

						while (true) {
							/*Indice pour fermer le socket */
							if (flag == false)
								break;
							
							//lire depuis le serveur
							in.read(b);
							System.out.println("#: " + new String(b));

							b = new byte[35];
						}

						try {
							Thread.sleep(20);
						} catch (InterruptedException ex) {
							Logger.getLogger(ChatClient1.class.getName()).log(Level.SEVERE, null, ex);
						}

						in.close();  //fermer

					} catch (IOException ex) {
						Logger.getLogger(ChatClient1.class.getName()).log(Level.SEVERE, null, ex);
					}

				

			} catch (IOException ex) {
				Logger.getLogger(MessageReceptor.class.getName()).log(Level.SEVERE, null, ex);
			}

		}

	}

	/**
	 * Cette fonction main() utilise une boucle infinie pour recevoir les messages depuis le terminal jusque "quit" <br>
	 * et chaque instance de thread secondaire communique avec le serveur <br>
	 * @param args  
	 * 			String[] args   On peut le récupérer sur le terminal.
	 * @return void  
	 * 			Il n'a pas de valeur retour.
	 * @exception IOException 
	 * 			IO erreur.
	 */
	public static void main(String[] args) {

		try {
			Socket client = new Socket("localhost", 10080);   //socket en port 10080

			MessageReceptor msgReceptor = new MessageReceptor(client);

			msgReceptor.start();

			OutputStream out = client.getOutputStream();

			/*Obtenir l'entrée par le terminal    */
			System.out.println("Entrez votre pseudo: ");
			Scanner sc = new Scanner(System.in);
			String entreeClient = sc.nextLine();

			out.write((entreeClient).getBytes());

			while (!entreeClient.equals("quit")) {     //Terminer si recevoir "quit"

				entreeClient = sc.nextLine();
				/*Ecrire sur le channel  */
				out.write((entreeClient).getBytes());

				if (entreeClient.equals("quit")) {
					msgReceptor.flag = false;
				}

			}

			out.write("quit".getBytes());
			out.close();

			client.close();

			System.out.println("FIN");

		} catch (IOException ex) {
			Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
