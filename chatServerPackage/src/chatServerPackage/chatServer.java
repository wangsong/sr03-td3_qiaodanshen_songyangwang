package chatServerPackage;

import java.net.ServerSocket;
import java.net.Socket;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.io.*;
import java.net.*;

/**
 * @author Songyang WANG
 * @author Qiaodan SHEN
 * @version 1.0.0	28/03/2022
 * Cette classe est pour initialiser le serveur de tous les clients en utilisant multi-thread 
 * il contient la fonction main() comme thread principal et ThreadedEchoHandler.start() comme thread secondaire
 */

public class chatServer {

	public static ArrayList<Socket> socketList = new ArrayList<Socket>();
	public static ArrayList<String> noms = new ArrayList<>();
	// public static int nomRecu=0;

	/**
	 * Cette classe est pour lire les messages depuis le client et les envoyer aux tous les clients par channel
	 */
	public static class ThreadedEchoHandler implements Runnable {
		private Socket socket = null;
		public int bienvenueRecu = 0;
		public int nomRecu = 0;
		public String name = "";

		/*Constructeur*/
		public ThreadedEchoHandler(Socket s) {
			this.socket = s;
			System.out.println(this.socket);
		}

		/**
		 * Démarrer l'exécution du thread <br>
		 * chaque client utilise son propre channel pour communiquer avec le serveur <br>
		 * chaque fois qu'on reçoit un message, on le distribue vers tous les clients en écrivant sur tous les channels <br>
		 * Il termine s'il reçoit le message "quit"
		 */
		@Override
		public void run() {
			InputStream in;
			try {
				in = socket.getInputStream();
				OutputStream out = socket.getOutputStream();
				out.write(("Entrez votre pseudo: ").getBytes());

				byte b[] = new byte[50];
				String MessageClient;   //Message vient du client

				do {
					
					try{
						in.read(b);
					}catch (SocketException se) {     //Une déconnextion imprévue
						// TODO Auto-generated catch block
						se.printStackTrace();
						this.socket.close(); 
						
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						for (int i = socketList.size()-1; i >= 0; i--){

						    if (this.socket.equals(socketList.get(i))){
						        // supprimer
						    	socketList.remove(socketList.get(i));
						    	System.out.println("Supprimer "+this.socket);
						    	System.out.println(socketList);
						    }
						}
						
						for (int i = noms.size()-1; i >= 0; i--){

						    if (this.name.equals(noms.get(i))){
						        // supprimer
						    	System.out.println(noms.get(i)+" a quitté ce chatroom ");
						    	System.out.println("********************************");
						    	//out.write((noms.get(i)+" a quitté ce chatroom ").getBytes());   //?
						    	for (int j = 0; j < socketList.size(); j++) {
									socket = socketList.get(j); 
									out = socket.getOutputStream();

									out.write((noms.get(i)+" a quitté ce chatroom ").getBytes());
									out.write("********************************".getBytes());
	
								}
						    	noms.remove(noms.get(i));
						    	break;
						    }
						}

						System.out.println(noms);

						break;

					}

					MessageClient = new String(b); 
					MessageClient.trim(); 

					/*
					     nomRecu==0 : On n'a pas reçu le nom de ce client
					     Si on a recu, on met nomRecu=1
					*/
					if (this.nomRecu == 0) { 
						
						this.name = MessageClient.trim();
						
						int doublon=0;   //pas de doublon en noms
						for(int k = 0;k < noms.size(); k ++){
				            System.out.println(noms.get(k));
				            if(noms.get(k).equals(this.name)) {
				            	doublon=doublon+1;
				            }
				        }
						if(doublon!=0) {
							out.write(("Il y a un doublon, Votre nouveau pseudo est: "+ this.name+(doublon+1)).getBytes() );
							doublon=doublon+1;
							noms.add(this.name+(doublon));
							this.name=this.name+(doublon);
						} else {
							noms.add(this.name);
						}
						
						
						this.nomRecu=1;
						System.out.println("noms: " + noms);
					}

					for (int i = 0; i < socketList.size(); i++) {
						Socket socket = socketList.get(i);

						if (!socket.isClosed()) {
							out = socket.getOutputStream();
							System.out.println("size " + socketList.size());
							System.out.println(socketList);
							//System.out.println(this.bienvenueRecu); 
							
							/*
						     bienvenueRecu==0 : On n'a pas envoyé le bienvenue du nouveau client
						     Si on a recu et envoyé, on met bienvenueRecu=1
							 */
							try {
								Thread.sleep(20);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if (this.bienvenueRecu == 0) { 

								for (int j = 0; j < socketList.size(); j++) {
									socket = socketList.get(j); 
									out = socket.getOutputStream();

									out.write((this.name + " a rejoint la conversation").getBytes());
									out.write("-----------------------------------".getBytes());

									System.out.println(this.name + " a rejoint la conversation");
									System.out.println("-----------------------------------");
								}

								this.bienvenueRecu++;
								break;
							}

							out.write((this.name.trim() + " a dit: " + MessageClient.trim()).getBytes());

						}

					}

					//if (MessageClient.equalsIgnoreCase("quit"))  break;
					if (MessageClient.startsWith("quit")) {
						this.socket.close();
						for (int i = socketList.size()-1; i >= 0; i--){

						    if (this.socket.equals(socketList.get(i))){
						        // supprimer
						    	socketList.remove(socketList.get(i));
						    	System.out.println("Supprimer "+this.socket);
						    }
						}
						System.out.println(socketList);
						
						for (int i = noms.size()-1; i >= 0; i--){

						    if (this.name.equals(noms.get(i))){
						        // supprimer
						    	System.out.println(noms.get(i)+" a quitté ce chatroom ");
						    	out.write("********************************".getBytes());
						    	
						    	//out.write((noms.get(i)+" a quitté ce chatroom ").getBytes());
						    	
						    	for (int j = 0; j < socketList.size(); j++) {
									socket = socketList.get(j); 
									out = socket.getOutputStream();

									out.write((noms.get(i)+" a quitté ce chatroom ").getBytes());
									out.write("********************************".getBytes());
	
								}
						    	
						    	noms.remove(noms.get(i));
						    	
						    	
						    }
						}

						System.out.println(noms);
						
					}

					b = new byte[50]; 

				} while (!MessageClient.startsWith("quit"));    //Boucle jusqu'à recevoir "quit"

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * Cette fonction main() utilise une boucle infinie pour attendre des nouveaux clients
	 * et chaque instance de thread secondaire communique avec le client correspondant <br>
	 * @param args  
	 * 			String[] args   On peut le récupérer sur le terminal.
	 * @return void  
	 * 			Il n'a pas de valeur retour.
	 * @exception IOException 
	 * 			IO erreur.
	 */
	public static void main(String[] args) {

		try {

			ServerSocket serverSocket = new ServerSocket(10080); //socket en port 10080
			/*Boucle infinie pour attendre un nouveau client  */
			while (true) {
				Socket socket = serverSocket.accept(); 
				//socket.setSoTimeout(10*1000);     //
				socketList.add(socket); 
				System.out.println(socketList);

				ThreadedEchoHandler handler = new ThreadedEchoHandler(socket);
				Thread thread = new Thread(handler); 

				thread.start();    //Démarrer un nouveau thread pour communiquer avec ce nouveau client
			}

		} catch (IOException ex) { 
			Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
